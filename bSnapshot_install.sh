echo ======================================
echo Downloading a baking-snapshot binary
echo ======================================
TEMP_DIR=$(mktemp -d)
PREFIX=${2-"/usr/local/bin"}
curl -L https://gitlab.com/tezoskorea/baking-snapshots/raw/master/baking-snapshots-linux.tar.gz -o ${TEMP_DIR}/baking-snapshots.tar.gz

echo ======================================
echo Untar a baking-snapshot binary
echo ======================================
tar -zxvf ${TEMP_DIR}/baking-snapshots.tar.gz -C ${TEMP_DIR}
sudo cp ${TEMP_DIR}/baking-snapshots-linux ${PREFIX}/baker-snapshots
rm -fr ${TEMP_DIR} || exit 1

echo "Finished!"
