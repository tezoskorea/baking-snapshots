const fs = require('fs');
const program = require('commander');
const { execSync } = require('child_process');

// const HOME = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
// const config = require(`${HOME}/.baking-snapshots/config.json`);
// const { delegate, host, port } = config;
const LOCAL_HOST = '127.0.0.1';
const DEFAULT_PORT = '8732';
const CURL = `curl -s http://`;
const CURL_HTTPS = `curl -s https://`;
var RPC_ADDR = '';

const BY_PASS = data => data;

const curl = (url, filter) => {
  let result = '';
  return new Promise((resolve, reject) => {
    // console.log(`${CURL}${RPC_ADDR}${url}`)
    result = execSync(`${CURL}${RPC_ADDR}${url}`);
    result = result.toString();
    result = JSON.parse(result);
    result = filter(result)
    resolve(result);
  })
  .catch(err => {
    console.log(result);
    process.exit(1);
  });
}

const getBakingRights = (cycle, cp, head, delegate) => {
  const bRightsRecordBegin = (cycle - 5) * 4096 + 1;
  const bRightsRecordEnd = (cycle + 6) * 4096 - 1;

  if ((bRightsRecordEnd < cp) || (head < bRightsRecordBegin)) {
    console.log(
      `Your snapshot file is too recent!\n`,
      `\bBaking rights are recored at level ${bRightsRecordBegin} ~ level ${bRightsRecordBegin}.\n`,
      `\bYour snapshot file has level ${cp} ~ level ${head}`);
    process.exit(1);
  };

  // if (bRightsRecordBegin < cp) then
  // 1) cp == bRightsRecordEnd or
  // 2) bRightsRecordEnd <= head or
  // 3) bRightsRecordEnd > head
  // in all cases, cp can be used
  // else if (cp < bRightsRecordBegin) then
  // use bRightsRecordBegin
  const bRightsIdxRecordLevel =  (bRightsRecordBegin < cp) ? cp : bRightsRecordBegin;
  const url = `/chains/main/blocks/${bRightsIdxRecordLevel}/helpers/"baking_rights?cycle=${cycle}&delegate=${delegate}&max_priority=1"`;
  return curl(url, data => data.map(d => d.level));
}

const getEndorsingRights = (cycle, cp, head, delegate) => {
  const bRightsRecordBegin = (cycle - 5) * 4096 + 1;
  const bRightsRecordEnd = (cycle + 6) * 4096 - 1;

  if ((bRightsRecordEnd < cp) || (head < bRightsRecordBegin)) {
    console.log(
      `Your snapshot file is too recent!\n`,
      `\bBaking rights are recored at level ${bRightsRecordBegin} ~ level ${bRightsRecordBegin}.\n`,
      `\bYour snapshot file has level ${cp} ~ level ${head}`);
    process.exit(1);
  };

  // if (bRightsRecordBegin < cp) then
  // 1) cp == bRightsRecordEnd or
  // 2) bRightsRecordEnd <= head or
  // 3) bRightsRecordEnd > head
  // in all cases, cp can be used
  // else if (cp < bRightsRecordBegin) then
  // use bRightsRecordBegin
  const bRightsIdxRecordLevel =  (bRightsRecordBegin < cp) ? cp : bRightsRecordBegin;
  const url = `/chains/main/blocks/${bRightsIdxRecordLevel}/helpers/"endorsing_rights?cycle=${cycle}&delegate=${delegate}"`;
  return curl(url, data => data.map(d => ({ level: d.level, slots: d.slots })));
}

const getDelegatorBalance = (snapshotLevel, delegator) => curl(
    `/chains/main/blocks/${snapshotLevel}/context/contracts/${delegator}`,
    data => Number(data.balance));

const getDelegatorsAtSnapshot = snapshotLevel => curl(
    `/chains/main/blocks/${snapshotLevel}/context/delegates/${DELEGATE}`,
    data => (
      {
        staking_balance: Number(data.staking_balance),
        delegated_balance: Number(data.delegated_balance),
        delegated_contracts: data.delegated_contracts
      }
    ));

// get snapshot index
const getSnapshotIdx = (cycle, cp, head) => {
  const ssIdxRecordBegin = (cycle - 5) * 4096;
  const ssIdxRecordEnd = (cycle + 6) * 4096 - 1;

  if ((ssIdxRecordEnd < cp) || (head < ssIdxRecordBegin)) {
    console.log(
      `Your snapshot file is too recent!\n`,
      `\bCycle ${cycle}'s snapshot was cycle ${cycle - 7}.\n`,
      `\bSnapshotIdx is recored at level ${ssIdxRecordBegin} ~ level ${ssIdxRecordEnd}.\n`,
      `\bYour snapshot file has level ${cp} ~ level ${head}`);
    process.exit(1);
  };

  // if (ssIdxRecordBegin < cp) then
  // 1) cp == ssIdxRecordEnd or
  // 2) ssIdxRecordEnd <= head or
  // 3) ssIdxRecordEnd > head
  // in all cases, cp can be used
  // else if (cp < ssIdxRecordBegin) then
  // use ssIdxRecordBegin
  const ssIdxRecordLevel =  (ssIdxRecordBegin < cp) ? cp : ssIdxRecordBegin;
  const url = `/chains/main/blocks/${ssIdxRecordLevel}/context/raw/json/cycle/${cycle}`;
  return curl(url, data => Number(data.roll_snapshot));
}

const getEarnedRewardsFilter = cycleInfo => data => {
  const rewards = data[data.length - 1].rewards;
  console.log(`${rewards} are earned in cycle ${cycleInfo.cycle}`)
  return Object.assign(cycleInfo, { rewards });
}
const getEarnedRewards = cycleInfo => curl(
    `/chains/main/blocks/${(cycleInfo.cycle + 1) * 4096 + 1}/context/delegates/${DELEGATE}/frozen_balance_by_cycle`,
    getEarnedRewardsFilter(cycleInfo));

const getCurrentHead = () => curl(
    '/chains/main/blocks/head/metadata',
    data => Number(data.level.level)
  )

const getCheckpoint = () => curl(
    '/chains/main/checkpoint',
    data => Number(data.block.level)
  )

const range = (start, end) => [...Array(end - start + 1)].map((_, i) => start + i);

// 사이클 C의 베이킹 스냅샷은
// C-7 사이클의 밸런스 스냅샷 중 하나로 결정되고
// C-6 사이클 마지막에 결정 된다
// C-6 사이클은
// (C-6) * 4096 + 1 부터 시작해서
// (C-5) * 4096 에 끝난다

// snapshotIdx
// (C - 5) * 4096에 처음 기록되고
// (C + 6) * 4096 -1 까지 기록된다

// {baking,endorsing}_rights
// (C - 5) * 4096에 + 1처음 기록되고
// (C + 6) * 4096 -1 까지 기록된다


async function getSnapshot(cycle, delegate) {
  RPC_ADDR = program.rpcAddr || `${LOCAL_HOST}:${DEFAULT_PORT}`;
  DELEGATE = delegate;
  TARGET_CYCLE = cycle;

  const checkpoint = await getCheckpoint();
  const cpCycle = parseInt((checkpoint - 1) / 4096);
  const bakingCycleBegin = cpCycle + 7;
  const head = await getCurrentHead();
  const currentCycle = parseInt((head - 1) / 4096);
  const bakingCyclesEnd = currentCycle - 1 + 6;

  const snapshotIdx = await getSnapshotIdx(TARGET_CYCLE, checkpoint, head);
  const snapshotLevel = (cycle - 7) * 4096 + 256 * (snapshotIdx + 1);

  if (((snapshotLevel < checkpoint) || (head < snapshotLevel))) {
    console.log(
      `Your snapshot file is too recent!\n`,
      `\bCycle ${cycle}'s snapshot was a snapshot ${snapshotIdx} in cycle ${cycle - 7},\n`,
      `\bwhich is level ${snapshotLevel}.\n`,
      `\bYour snapshot file has level ${checkpoint} ~ level ${head}`);
    process.exit(1);
  }

  console.log({ DELEGATE, TARGET_CYCLE, RPC_ADDR, checkpoint, cpCycle, head, currentCycle, bakingCycleBegin, bakingCyclesEnd });
  const { staking_balance, delegated_balance, delegated_contracts } = await getDelegatorsAtSnapshot(snapshotLevel);

  console.log("=====================================")
  console.log(`calculate rewards in cycle ${TARGET_CYCLE}` );
  console.log(`snapshotLevel: ${snapshotLevel}`);
  console.log(`staking balace: ${staking_balance}`);
  console.log(`delegated balace: ${delegated_balance}`);
  console.log('delgators:');
  console.log(delegated_contracts);

  const delegations = await delegated_contracts.reduce(async (acc, delegator) => {
    const balance = await getDelegatorBalance(snapshotLevel, delegator);
    console.log(`delegator ${delegator}'s balance is ${balance}'`);
    return [...(await acc), {delegator, balance}];
  }, []);

  // baking & endorsing schedule
  var baking_rights = await getBakingRights(TARGET_CYCLE, checkpoint, head, DELEGATE);
  baking_rights = {
    numbers: baking_rights.length,
    blocks: baking_rights
  };
  console.log("=====================================")
  console.log(`baking_rights: ${baking_rights.numbers}`);
  console.log(baking_rights.blocks);

  var endorsing_rights = await getEndorsingRights(TARGET_CYCLE, checkpoint, head, DELEGATE);
  endorsing_rights = {
    numbers: endorsing_rights.reduce((acc, level) => acc + level.slots.length, 0),
    blocks: endorsing_rights
  };
  console.log(`endorsing_rights: ${endorsing_rights.numbers}`);
  console.log(endorsing_rights.blocks);
  console.log("=====================================")
  const estimated_rewards = baking_rights.numbers * 16 + endorsing_rights.numbers * 2;
  console.log(`estimated_rewards: ${estimated_rewards}`);


  const result = {
    cycle,
    snapshotIdx,
    snapshotLevel,
    staking_balance,
    delegated_balance,
    delegated_contracts,
    delegations,
    estimated_rewards,
    baking_rights,
    endorsing_rights,
  }

  // save the result
  fs.writeFileSync(
    `./CYCLE${TARGET_CYCLE}_${DELEGATE}.json`,
    JSON.stringify(result, null, 2)
  );
}

const sanity = {
  // 'set': (key, val) => Object.keys(config).includes(key),
  'snapshot': (w1, cycle, w2) => {
    return (
      w1 === 'snapshot' &&
      w2 === 'for' &&
      Number.isInteger(Number(cycle))
    )
  }
}

const sanityCheck = (type, args) => sanity[type](...args);

program
  .option('-r, --rpc-addr <rpc:port>')

// program
//   .command('set <key> <value>')
//   .action((key, value) => {
//     if (sanityCheck('set', [key, value]) == false) {
//       console.log(`${key} is invalid key of configuration!`);
//       process.exit(1);
//     }
//     config[key] = value;
//     fs.writeFileSync(`${HOME}/.baking-snapshots/config.json`, JSON.stringify(config, null, 2))
//   });
//
program
  .command('get <snapshot> <cycle> <for> <delegate>')
  .action((w1, cycle, w2, delegate) => {
    if (sanityCheck('snapshot', [w1, cycle, w2]) === false) {
      console.log("Wrong command!");
      console.log("Usage: get snapshot 123 for tz1...");
      process.exit(1);
    };
    getSnapshot(Number(cycle), delegate);
  });

program.parse(process.argv);
